<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Address extends Authenticatable
{
    protected $table = 'address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','name','phone_number','line1','line2','line3','pincode','city','state','country','type','is_default','user_id',
    ];
}