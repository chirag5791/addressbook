<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Address;
use View;
use Validator;
use Illuminate\Support\Facades\Input;
use Auth;
use Redirect;
use DB;

class AddressController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        // get all the Address
        $Address = DB::table('address')
                ->where('address.user_id','=',$user->id)
                ->get();

        // load the view and pass the Address
        return View::make('Address.index')
            ->with('Address', $Address);
    }

    public function create()
    {
        return View::make('Address.create');
    }

    public function store()
    {
        $input = Input::all();
        $user = Auth::user();

        // validate
        $rules = array(
        	'title'      => 'required',
            'name'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) 
        {
            return Redirect::to('address/create')
                ->withErrors($validator);
        } else {
            if (isset($input['is_default']) && $input['is_default'] == 1) 
            {
                $this->checkDefaultValue($input['is_default'], $input['type'],$user->id);
            } else {
                $input['is_default'] = 0;
            }
            // store
            $Address = new Address;
            $Address->title = $input['title'];
            $Address->name = $input['name'];
            $Address->phone_number = $input['phone_number'];
            $Address->line1 = $input['line1'];
            $Address->line2 = $input['line2'];
            $Address->line3 = $input['line3'];
            $Address->pincode = $input['pincode'];
            $Address->city = $input['city'];
            $Address->state = $input['state'];
            $Address->country = $input['country'];
            $Address->type = $input['type'];
            $Address->is_default = $input['is_default'];
            $Address->user_id = $user->id;
            $Address->save();

            return Redirect::to('address');
        }
    }

    public function checkDefaultValue($is_default,$type,$user_id)
    {
        $result = DB::table('address')
                ->where('address.is_default','=', 1)
                ->where('address.type','=',$type)
                ->where('address.user_id','=',$user_id)
                ->select('address.id')
                ->get();
        if(!empty($result)) 
        {
            DB::table('address')->where('id', '=', $result[0]->id)->update(array('is_default' => 0));
        }
    }

    public function show($id)
    {
        // get the Address
        $Address = Address::find($id);

        // show the view and pass the Address to it 
        return View::make('Address.edit')
            ->with('Address', $Address);
    }

    public function edit($id)
    {
        // get the Address
        $Address = Address::find($id);

        // show the edit form and pass the Address
        return View::make('Address.edit')
            ->with('Address', $Address);
    }

    public function update($id)
    {
        $input = Input::all();
        $user = Auth::user();

        // validate
        $rules = array(
            'title'      => 'required',
            'name'       => 'required', 
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) 
        {
            return Redirect::to('address')
                ->withErrors($validator);
        } else {
            if (isset($input['is_default']) && $input['is_default'] == 1) 
            {
                $this->checkDefaultValue($input['is_default'], $input['type'],$user->id);
            } else {
                $input['is_default'] = 0;
            }
            // store
            $Address = Address::find($id);
            $Address->title = $input['title'];
            $Address->name = $input['name'];
            $Address->phone_number = $input['phone_number'];
            $Address->line1 = $input['line1'];
            $Address->line2 = $input['line2'];
            $Address->line3 = $input['line3'];
            $Address->pincode = $input['pincode'];
            $Address->city = $input['city'];
            $Address->state = $input['state'];
            $Address->country = $input['country'];
            $Address->type = $input['type'];
            $Address->is_default = $input['is_default'];
            $Address->user_id = $user->id;
            $Address->save();

            return Redirect::to('address');
        }
    }

    public function destroy($id)
    {
        // delete
        $Address = Address::find($id);
        $Address->delete();
        
        return Redirect::to('address');
    }
}
