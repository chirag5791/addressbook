@extends('layouts.app')

@section('content')
<div class="container">

    <a class="btn btn-small btn-info pull-right" href="{{ URL::to('address/create') }}">Create a Address</a>

    <h2>All the address</h2>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Title</td>
                <td>Name</td>
                <td>Phone Number</td>
                <td>Address</td>
                <td>Type</td>
                <td>Default</td>
                <td colspan="2">Action</td>
            </tr>
        </thead>
        <tbody>
        @foreach($Address as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->title }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->phone_number }}</td>
                <td>{{ $value->line1 }} {{ $value->line2 }} {{ $value->line3 }}</td>
                <td>{{ $value->type }}</td>
                <td>
                    @if($value->is_default == 0)
                       {{ 'NO' }}
                    @else
                        {{ 'YES' }}
                    @endif
                </td>
                <td>
                    <a class="btn btn-small btn-info" href="{{ URL::to('address/' . $value->id . '/edit') }}">Edit</a>
                </td>
                <td>
                    <form action="{{action('AddressController@destroy', $value->id)}}" method="post">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection