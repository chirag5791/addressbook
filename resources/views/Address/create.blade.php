@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create a Address</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/address') }}">
                        <input type="hidden" value="{{csrf_token()}}" name="_token" />

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone_number" class="col-md-4 control-label">Phone Number</label>
                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="line1" class="col-md-4 control-label">Address Line1</label>
                            <div class="col-md-6">
                                <input id="line1" type="text" class="form-control" name="line1" value="{{ old('line1') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="line2" class="col-md-4 control-label">Address Line2</label>
                            <div class="col-md-6">
                                <input id="line2" type="text" class="form-control" name="line2" value="{{ old('line2') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="line3" class="col-md-4 control-label">Address Line3</label>
                            <div class="col-md-6">
                                <input id="line3" type="text" class="form-control" name="line3" value="{{ old('line3') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pincode" class="col-md-4 control-label">Pincode</label>
                            <div class="col-md-6">
                                <input id="pincode" type="text" class="form-control" name="pincode" value="{{ old('pincode') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="city" class="col-md-4 control-label">City</label>
                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="state" class="col-md-4 control-label">State</label>
                            <div class="col-md-6">
                                <input id="state" type="text" class="form-control" name="state" value="{{ old('state') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="country" class="col-md-4 control-label">Country</label>
                            <div class="col-md-6">
                                <input id="country" type="text" class="form-control" name="country" value="{{ old('country') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="type" class="col-md-4 control-label">Type</label>
                            <div class="col-md-6">
                                <select class="form-control" name="type" value="{{ old('type') }}">
                                    <option value="Default From">Default From</option>
                                    <option value="Default To">Default To</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="is_default" class="col-md-4 control-label">Is Default</label>
                            <div class="col-md-6">
                                <input name="is_default" type="checkbox" value="1">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
